'use strict';
const Chain = require('./chain')

const chain = new Chain()

function addData(req, res){
  chain.addData(req.body.data);

  res.json({message: 'ok'});
}

function lastBlocks(req, res){
  let n = parseInt(req.params.n);
  if(!isNaN(parseInt(n)) && isFinite(n)){
    res.json(chain.last(n));
  } else {
    res.json({message: 'n must be integer'});
  }
}

module.exports = {
  addData: addData,
  lastBlocks: lastBlocks
};
