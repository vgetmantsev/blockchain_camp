'use strict';

module.exports = function(app) {
  const controller = require('./controller')
  app.route('/add_data')
    .post(controller.addData);


  app.route('/last_blocks/:n')
    .get(controller.lastBlocks);
};
