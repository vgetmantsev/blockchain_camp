const Block = require('./block')

class Chain {
  constructor(){
    this._blocks = {}
    this.currentBlock = new Block(0);
  }

  last(n){
    let block = this.currentBlock
    let blocks = []
    while(n > 0 && (block = this._blocks[block.previous_block_hash])){
      blocks.push(block)
      --n;
    }

    return blocks.reverse();
  }

  addData(_data){
    let current = this.currentBlock;
    current.addData(_data);

    if(current.closed()){
      this._blocks[current.block_hash] = current

      this.currentBlock = new Block(current.block_hash)
    }
  }
}

module.exports = Chain
