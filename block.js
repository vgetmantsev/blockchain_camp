const MAX_BLOCK_LENGTH = 5;
const crypto = require('crypto');

class Block {
  constructor(_prev){
    this.previous_block_hash = _prev || 0
    this.rows = []
    this.timestamp = null
  }

  addData(_data){
    if(!this.closed()){
      this.rows.push(_data)

      if(this.full()){
        const hash = crypto.createHash('sha256')

        this.timestamp = new Date().getTime()

        hash.update(JSON.stringify(this))
        this.block_hash = hash.digest('hex')
      }
    } else {
      throw new Error('block is closed')
    }
  }

  full(){
    return this.rows.length === MAX_BLOCK_LENGTH
  }

  closed(){
    return !!this.block_hash
  }
}

module.exports = Block
